import axios from "axios"
import { getToken, clearToken } from './token'
import { history } from './history'

const http = axios.create({
  baseURL: 'https://www.fastmock.site/mock/f98dfaf6d0215e459a8c2f00d723cdc3/jsthin',
  timeout: 5000
})

// 请求拦截器
http.interceptors.request.use(config => {
  const token = getToken()
  if (token) {
    config.headers.Authorization = 'bearer ' + token
  }
  return config
}, error => {
  return Promise.reject(error)
})

// 响应拦截器
http.interceptors.response.use(response => {
  // 2xx状态码触发该函数
  if (response.data.code === '200') {
    return response.data
  } else {
    return false
  }
}, error => {
  if (error.response.status === 401) {
    // 删除token
    clearToken()
    // 跳转登录页
    history.push('/login')
  }
  // 非2xx状态码触发该函数
  return Promise.reject(error)
})

export { http }