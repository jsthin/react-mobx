import { makeAutoObservable } from 'mobx'
import { http, getToken, setToken, clearToken } from '@/utils'

class LoginStore {
  // 声明数据
  token = getToken() || ''
  constructor() {
    makeAutoObservable(this)
  }

  // 声明action
  login = async ({mobile, password}) => {
    const { data } = await http.post('/api/user/login',{mobile, password})
    if (data) {
      this.token = data.accessToken
      setToken(this.token)
    }
  }
  logout = () => {
    this.token = ''
    clearToken()
  }
}

export { LoginStore }