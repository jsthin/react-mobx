import React from "react"
import { LoginStore } from "./login.Store"
import { UserStore } from './user.Store'
import { TodoListStore } from './todoList.Store'

class RootStore {
  constructor() {
    this.loginStore = new LoginStore()
    this.userStore = new UserStore()
    this.todoListStore = new TodoListStore()
  }
}

// 创建根实例
const rootStore = new RootStore()
// 注入context
const context = React.createContext(rootStore)
const useStore = () => React.useContext(context)

export { useStore }
