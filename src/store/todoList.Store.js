/**
 * @desc mobx todolist
 * @author jsthin
 */

import { makeAutoObservable } from 'mobx'
class TodoListStore {
  // 定义数据
  list = [
    {
      id: 1,
      title: 'Vue',
      isDone: false
    },
    {
      id: 2,
      title: 'React',
      isDone: true
    },
    {
      id: 3,
      title: 'Vant',
      isDone: false
    }
  ]
  // 数据响应式
  constructor() {
    makeAutoObservable(this)
  }
  // 定义action函数
  addList = (value) => {
    const taskItem = {
      id: new Date().getTime(),
      title: value,
      isDone: false
    }
    this.list.push(taskItem)
  }
  singleCheck(id, checked) {
    this.list.find(item => item.id === id).isDone = checked
  }
  checkAll(checked) {
    this.list.forEach(item => item.isDone = checked)
  }

  // 计算属性
  get isAll() {
    return this.list.every(item => item.isDone)
  }
  get completedCount() {
    return this.list.filter(item => item.isDone).length
  }

}

// 导出
export { TodoListStore }