import { makeAutoObservable } from 'mobx'
import { http } from '@/utils'

class UserStore {
  // 声明数据
  userInfo = {
    username: '',
    userType: 0
  }
  constructor() {
    makeAutoObservable(this)
  }

  // 声明action
  getUserInfo = async () => {
    const { data } = await http.post('/api/user/userInfo')
    if (data) {
      this.userInfo = data
    }
  }
}

export { UserStore }