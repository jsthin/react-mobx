import { useState } from 'react'
import { Card } from 'antd'
// 引入counterStore
import { useStore } from '@/store'
// 引入mobx中间件
import { observer } from 'mobx-react-lite'
import './index.css'

function Task() {
  const [textVal, setTextVal] = useState('')
  const { todoListStore } = useStore()

  /**
   * 单选回调
   * @param {Event} e 事件
   * @param {number} id
   */
  function checkChange(e, id) {
    todoListStore.singleCheck(id, e.target.checked)
  }

  /**
   * 全选回调
   * @param {Event} e 事件
   * @param {number} id
   */
   function allCheckChange(e) {
    todoListStore.checkAll(e.target.checked)
  }

  /**
   * 回车回调函数
   * @param {Event} e 回调事件参数
   */
  function handleKeyDown(e) {
    if(e.keyCode === 13) {
      todoListStore.addList(textVal)
      setTextVal('')
    }
  }

  return (
    <Card className="task-box">
      <div className="task-input">
        <label>
          <input
            type="checkbox"
            checked={todoListStore.isAll}
            onChange={allCheckChange}
          />
          全选
        </label>
        <input
          className="input"
          type="text"
          value={textVal}
          placeholder="输入任务，回车新增"
          onChange={e => setTextVal(e.target.value)}
          onKeyDown={handleKeyDown}
        />
      </div>
      <div className="task">
        {todoListStore.list.map(item => (
          <div className="task-item" key={item.id}>
            <label>
              <input
                type="checkbox"
                checked={item.isDone}
                onChange={e => checkChange(e, item.id)}
              />
              {item.title}
            </label>
          </div>
        ))}
        <div className="task-item">
          {todoListStore.completedCount}已完成/{todoListStore.list.length}总数
        </div>
      </div>
    </Card>
  );
}

export default observer(Task)
