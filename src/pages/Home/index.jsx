import Bar from '@/components/Bar'
import './index.scss'


function Home() {
  return (
    <div className='home'>
      <Bar
        style={{ width: '500px', height: '400px' }}
        barLabel={['vue', 'angular', 'react']}
        barData={[50, 60, 70]}
        title='三大框架满意度'
      />
      <Bar
        style={{ width: '500px', height: '400px' }}
        barLabel={['vue', 'angular', 'react']}
        barData={[3345639, 1096680, 15420684]}
        title='三大框架周下载量'
      />
    </div>
  )
}

export default Home