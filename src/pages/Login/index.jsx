import { Card, Form, Input, Checkbox, Button, message } from 'antd'
import { useStore } from '@/store'
import { useNavigate } from 'react-router-dom'
import logo from '@/assets/logo.png'
import './index.scss'

const Login = () => {
  // mobx
  const { loginStore } = useStore()
  const navigate = useNavigate()

  const onFinish = async (values) => {
    if (values.remember) {
      await loginStore.login({
        mobile: values.mobile,
        password: values.password
      })
      message.success('登录成功')
      // 跳转首页
      navigate('/', { replace: true })
    } else {
      message.error('请阅读并同意「用户协议」和「隐私条款」')
    }
  }

  return (
    <div className='login'>
      <Card className="login-container">
        <img className="login-logo" src={logo} alt="" />
        <Form
          initialValues={{ remember: true, mobile: 18812344321, password: 123456  }}
          validateTrigger={['onBlur', 'onChange']}
          onFinish={onFinish}
        >
          <Form.Item
            name="mobile"
            rules={[
              { pattern: /^1[3-9]\d{9}$/, message: "请输入正确的手机号", validateTrigger: "onBlur" },
              { required: true, message: '请输入手机号' }
            ]}
          >
            <Input size='large' placeholder='请输入手机号' />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[
              { pattern: /[0-9A-Za-z]{6,}$/, message: "最少6位字符", validateTrigger: "onBlur" },
              { required: true, message: '请输入密码' }
            ]}
          >
            <Input size='large' type="password" placeholder='请输入密码' />
          </Form.Item>
          <Form.Item name="remember" valuePropName="checked">
            <Checkbox className='login-checkbox-label'>
              我已阅读并同意「用户协议」和「隐私条款」
            </Checkbox>
          </Form.Item>
          <Form.Item>
            <Button type='primary' htmlType='submit' size='large' block>登录</Button>
          </Form.Item>
        </Form>
      </Card>
    </div>
  )
}

export default Login
