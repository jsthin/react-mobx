import * as echarts from 'echarts'
import { useEffect, useRef } from 'react'

function drawBar(node, barLabel, barData, title) {
  const barChart = echarts.init(node)
  const option = {
    title: {
      text: title
    },
    grid: {
      left: '15%'
    },
    tooltip: {},
    xAxis: {
      data: barLabel
    },
    yAxis: {},
    series: [
      {
        name: '满意度',
        type: 'bar',
        data: barData
      }
    ]
  }
  barChart.setOption(option)
}

function Bar({barLabel, barData, title, style}) {
  const barRef = useRef(null)
  useEffect(() => {
    drawBar(barRef.current, barLabel, barData, title)
  }, [barLabel, barData, title])
  return <div ref={barRef} style={style} />
}

export default Bar