import { Routes, Route } from 'react-router-dom'
import { HistoryRouter, history } from '@/utils'
import './App.css'
// 引入路由鉴权组件
import { AuthRoute } from '@/components/AuthRoute'
// 引入路由组件
import { lazy, Suspense } from 'react'
const Layout = lazy(() => import('@/pages/Layout'))
const Login = lazy(() => import('@/pages/Login'))
const Home = lazy(() => import('@/pages/Home'))
const Article = lazy(() => import('@/pages/Article'))
const Publish = lazy(() => import('@/pages/Publish'))
const Todo = lazy(() => import('@/pages/Todo'))


function App() {
  return (
    <HistoryRouter history={history}>
      <div className="App">
        {/* 懒加载组件 */}
        <Suspense
          fallback={
            <div
              style={{
                textAlign: 'center',
                marginTop: 200,
              }}
            >
              loading...
            </div>
          }
        >
          <Routes>
            <Route
              path="/"
              element={
                // 使用路由鉴权
                <AuthRoute>
                  <Layout />
                </AuthRoute>
              }
            >
              <Route index element={<Home />} />
              <Route path="article" element={<Article />} />
              <Route path="publish" element={<Publish />} />
              <Route path="todo" element={<Todo />} />
            </Route>
            <Route path="/login" element={<Login />} />
          </Routes>
        </Suspense>
      </div>
    </HistoryRouter>
  );
}

export default App;
